﻿using System;
using NotificationManagerCL;

namespace testappfornotificationmanager.dll
{
    internal class Program
    {
        private static void Main()
        {
            var notificationManager = new NotificationManager();

            var util = new RegexUtilities();
            int doing;

            do
            {
                Console.Clear();
                Console.WriteLine("1->Обработать сигнал");
                Console.WriteLine("2->Добавить адресата для сигнала");
                Console.WriteLine("3->Исключить адресата из сигнала");
                Console.WriteLine("4->Получить список поддерживаемых сигналов");
                Console.WriteLine("5->Добавить сигнал");
                Console.WriteLine("6->Проверка адресата на получения уведомления");
                Console.WriteLine("0->Выход");
                Console.Write("Действие: ");

                if (!int.TryParse(Console.ReadLine(), out doing))
                {
                    Console.WriteLine("Нужно вводить только числа!");
                    Console.WriteLine("Нажмите любую клавишу для продолжения...");
                    Console.ReadKey();
                    doing = -1;
                }
                else
                {
                    string code;
                    string email;
                    var signals = notificationManager.Signals;
                    switch (doing)
                    {
                        case 1:
                            Console.WriteLine("Введите код сигнала");
                            code = Console.ReadLine();
                            if (code != null && signals != null &&
                                signals.ContainsKey(code) &&
                                signals[code].Count != 0)
                            {
                                Console.WriteLine("Рассылка уведомления адресатам для кода {0}:", code);
                                notificationManager.HandleSignal(code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Ошибка! Такого кода не существует либо некому посылать");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 2:
                            Console.WriteLine("Введите код сигнала");
                            code = Console.ReadLine();
                            do
                            {
                                Console.WriteLine("Введите корректный email адресата");
                                email = Console.ReadLine();
                            } while (!util.IsValidEmail(email));

                            if (code != null && signals != null &&
                                signals.ContainsKey(code) &&
                                !signals[code].Contains(email))
                            {
                                notificationManager.AddDestinationForSignal(email, code);
                                Console.WriteLine("Успешно добавлен адресат с email {0} к сигналу {1}", email, code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine(
                                    "Ошибка! Такого кода не существует либо к такому коду уже привязан данный адресат");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 3:
                            Console.WriteLine("Введите код сигнала");
                            code = Console.ReadLine();
                            do
                            {
                                Console.WriteLine("Введите корректный email адресата");
                                email = Console.ReadLine();
                            } while (!util.IsValidEmail(email));
                            if (code != null && signals != null &&
                                signals.ContainsKey(code) &&
                                signals[code].Contains(email))
                            {
                                notificationManager.ExcludeDestinationForSignal(email, code);
                                Console.WriteLine("Успешно исключен адресат с email {0} от сигнала {1}", email, code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Ошибка! Такого кода не существует либо данный адресат не привязан");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 4:
                            if (signals != null && notificationManager.GetSignalList().Count != 0)
                            {
                                Console.WriteLine("Список сигналов:");
                                foreach (var wer in notificationManager.GetSignalList())
                                    Console.WriteLine("Сигнал с кодом: {0}", wer);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Ошибка!Нет сигналов с назначенными адресатами");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 5:
                            Console.WriteLine("Введите код сигнала");
                            code = Console.ReadLine();
                            if (code != null && signals == null)
                            {
                                notificationManager.AddSignal(code);
                                Console.WriteLine("Сигнал с кодом {0} успешно добавлен", code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else if (code != null && !signals.ContainsKey(code))
                            {
                                notificationManager.AddSignal(code);
                                Console.WriteLine("Сигнал с кодом {0} успешно добавлен", code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Ошибка! Такой код уже существует");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 6:
                            Console.WriteLine("Введите код сигнала");
                            code = Console.ReadLine();
                            do
                            {
                                Console.WriteLine("Введите корректный email адресата");
                                email = Console.ReadLine();
                            } while (!util.IsValidEmail(email));
                            if (code != null && signals != null &&
                                signals.ContainsKey(code))
                            {
                                var res = notificationManager.IsCheckDestinationForSignal(email, code);
                                Console.WriteLine(
                                    res
                                        ? "Адресат с email {0} участвует в рассылке уведомлений для сигнала с кодом {1}"
                                        : "Адресат с email {0} не участвует в рассылке уведомлений для сигнала с кодом {1}",
                                    email, code);
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Ошибка! Такого кода не существует");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                            }
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Выберите верный пункт!");
                            Console.WriteLine("Нажмите любую клавишу для продолжения...");
                            Console.ReadKey();
                            break;
                    }
                }
            } while (doing != 0);
        }
    }
}